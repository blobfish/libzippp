project('libzippp', 'cpp'
  , default_options : ['warning_level=0', 'cpp_std=c++11']
  , version : '6.0'
  , meson_version : '>=0.50'
)

# going to build both static and shared as this is small.

zlib = dependency('zlib', version : '>=1.2.11', required : true)
libzip = dependency('libzip', version : '>=1.7.0', required : true)

inc_dirs = include_directories(['src'], is_system : true)
install_headers('src/libzippp.h', subdir : 'libzippp')

libzippp_static = static_library('zippp'
  , 'src/libzippp.cpp'
  , include_directories : inc_dirs
  , dependencies : [zlib, libzip]
  , install : true
)

libzippp_shared = shared_library('zippp'
  , 'src/libzippp.cpp'
  , include_directories : inc_dirs
  , cpp_args : '-DLIBZIPPP_EXPORTS'
  , dependencies : [zlib, libzip]
  , install : true
)

pkg = import('pkgconfig')
pkg.generate(libzippp_shared
  , description : 'Simple C++ wrapper around the libzip library'
  , extra_cflags : 'cpp_std=c++11'
  , filebase : 'libzippp'
  , name : 'libzippp'
  , subdirs : 'libzippp')
  
if meson.is_subproject()
  libzippp_static_dep = declare_dependency(include_directories : inc_dirs
    , link_with : [libzippp_static]
  )
  libzippp_shared_dep = declare_dependency(include_directories : inc_dirs
    , link_with : [libzippp_shared]
  )
else
  static_test = executable('static_test'
    , 'tests/tests.cpp'
    , include_directories : inc_dirs
    , link_with : libzippp_static
  )
 shared_test = executable('shared_test'
    , 'tests/tests.cpp'
    , include_directories : inc_dirs
    , link_with : libzippp_shared
  )
endif
